FROM ruby:2.6.3-stretch

WORKDIR /project/
COPY . /project/

RUN apt-get update -qq && apt-get install -y postgresql-client

RUN gem install bundler \
  rubocop rubocop-md rubocop-performance rubocop-rails rubocop-rspec \
  rubycritic roodi fasterer \
  brakeman bundler-audit --no-document && bundle install

EXPOSE 3000
