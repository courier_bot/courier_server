A client URI must be provided to the server so that it can connect and request contact information.

See the project's `README.md` for more information.
