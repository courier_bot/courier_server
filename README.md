# COURIER SERVER

## WARNING

The `courier_project` Docker image is currently private.

The installation is under heavy construction. This currently assumes a Kubernetes cluster on Digital
Ocean (the persistent volume claim is currently set for a Digital Ocean Volume), and the Docker
image should be located in a Gitlab container registry (which is private). This will all be fixed in
a future release, including a public container.

## PREREQUISITES

A working kubernetes cluster and `kubectl` configuration are required, as well as the
[Helm](https://helm.sh/) package manager for kubernetes.

## INSTALLATION

The Helm chart can be found in the `.helm/` folder. The `values.yaml` inside shows the values that
need to be provided in a values file to the installer, and how to find them.

The Courier Server can be installed by executing the following:

    $ helm install --generate-name --values values.yaml ./.helm/courier-server

This will install two replicas of the API behind a load balancer on port 3000, four replicas of the
deliverer service, one replica of the periodic deliverer, a postgres database and a redis data
store, both on [DigitalOcean volumes](https://www.digitalocean.com/docs/volumes/).

## POST-INSTALLATION

After installation, a kubernetes job will setup the database automatically. Once
this is done, all pods should soon be running and ready.

The only thing left is to tell the Courier Server how to reach the test project
when it needs to request contact information. As the Courier Server's admin UI
is not coded yet, the following steps must be taken:

* connect to one of the Courier Server's API pods - `kubectl exec {CS_POD} -ti -- bundle exec rails
console`
* find the correct `Client` object and set its `uri` attribute to the correct client route that
should be used to retrieve contact information.

Alternately, the value can be changed manually in the database.

## Usage

A POST request can be sent to the server at the `/messages` route with the following body structure:

    {
      "message_kind": "order_created",
      "target_messageable": "user_12345",
      "context": null
    }

The message kind must of course be valid in the Courier Server, as per `MessageKind`'s `code`
attribute. The target messageable value is arbitrary and should only mean something to the client.
The context can be used to separate delivered messages.

The server will then make a request to the client's uri and ask for contact information. Based on
the response, the server will use the appropriate courier handler to deliver the message.

## LOCAL DEVELOPMENT

This application has the following local prerequisites:

* [Postgresql](https://www.postgresql.org/) as the database. The following commands can be used to
create a new user (role) and create the database:

      $ createuser --createdb --login --encrypted --pwprompt courier_server
      $ bundle exec rails db:create

  Change the `database.yml` file as needed for other databases.

* [Redis](https://redis.io/) as the key-value store. It is used by
[Resque](https://github.com/resque/resque) to keep track of background jobs.

* An account with [SendGrid](https://sendgrid.com/), used as an email service.

* An account with [Twilio](https://twilio.com/), used as a text message and phone call service.

* A `SENDGRID_DEFAULT_FROM_EMAIL_ADDRESS` environment variable set to the default `from` email
address.

* A `TWILIO_DEFAULT_FROM_PHONE_NUMBER` environment variable set to the default `from` phone number.

* `bundle exec rails credentials:edit` will generate a new master key and credentials file -
`credentials.yml.sample` lists what must be in the file. See `bundle exec rails credentials:help`
for more information about Rails credentials.

* `bundle install` to install the project's gem dependencies.

## HOW IT WORKS

* The communications will be sent using background jobs. The following commands will run the redis
server (if it's not set to run automatically), and start the background workers and a running task.
See [Resque Gem](https://github.com/resque/resque#workers) for more information about resque
workers.

      $ redis-server
      $ QUEUE=critical,high,normal,low bundle exec rake environment resque:work
      $ bundle exec rake periodic_message_deliveries

* In another terminal, start the server normally.

      $ bundle exec rails server

* The worker monitoring system can be started with the `resque-web` command. It will open a browser,
unless the `-L` flag is specified.

      $ resque-web config/initializers/resque.rb

Example requests with the server listening on port 3000:

* POST /messages

      $ curl -X POST \
        http://localhost:3000/messages \
        -H 'Content-Type: application/json' \
        -d '{ "message_kind": "order_created",
              "target_messageable": "user_236" }'

  The `message_kind` defines what happened so that a message needed to be sent. It is set-up in the
  courier server and the related clients.

  The `target_messageable` is a string uniquely representing a user, or anything that could receive
  a communication. The courier server has no knowledge about what the string means - it is only
  relevant to the client.

## Continuous Integration

This section contains links to the latest CI results on `master`.

### Style Checking

* [rubocop](http://zyrthofar.com/courier_bot/courier_server/master/outputs/rubocop.txt)
* [rubycritic](http://zyrthofar.com/courier_bot/courier_server/master/outputs/rubycritic.txt)
* [fasterer](http://zyrthofar.com/courier_bot/courier_server/master/outputs/fasterer.txt)

### Vulnerability Scanning

* [brakeman](http://zyrthofar.com/courier_bot/courier_server/master/outputs/brakeman.txt)
* [bundler-audit](http://zyrthofar.com/courier_bot/courier_server/master/outputs/bundler_audit.txt)
* [bundler-audit outdated gems](http://zyrthofar.com/courier_bot/courier_server/master/outputs/outdated_gems.txt)

### Testing

* [rspec](http://zyrthofar.com/courier_bot/courier_server/master/outputs/rspec.txt)
* [mutant](http://zyrthofar.com/courier_bot/courier_server/master/outputs/mutant.txt)
