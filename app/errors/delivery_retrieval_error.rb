# frozen_string_literal: true

# An error indicating a problem when retrieving a delivery.
class DeliveryRetrievalError < RecordRetrievalError; end
