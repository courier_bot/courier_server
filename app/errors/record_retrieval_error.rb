# frozen_string_literal: true

# Base error class for a problem when retrieving a record.
class RecordRetrievalError < StandardError; end
