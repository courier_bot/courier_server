# frozen_string_literal: true

# An error indicating that a handler type could not be found in `CourierHandler::BaseHandler`'s
# descendant classes.
class HandlerTypeUnknownError < StandardError; end
