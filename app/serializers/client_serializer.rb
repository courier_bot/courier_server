# frozen_string_literal: true

# Serializer for `Client` records.
class ClientSerializer < ApplicationSerializer
  # Serializes a client.
  def serialize
    {
      id: record.id,
      code: record.code,
      uri: record.uri,
      created_at: record.created_at.iso8601,
      updated_at: record.updated_at.iso8601,
      deleted_at: record.deleted_at&.iso8601
    }
  end
end
