# frozen_string_literal: true

# Serializer for `Message` records.
class MessageSerializer < ApplicationSerializer
  # Serializes a message.
  def serialize
    {
      id: record.id,
      message_kind: MessageKindSerializer.new(record.message_kind).serialize,
      target_messageable: record.target_messageable,
      context: record.context,
      status: record.status,
      created_at: record.created_at.iso8601,
      deliveries: deliveries
    }
  end

  private

  def deliveries
    record
      .deliveries
      .order(:created_at)
      .map { |delivery| DeliverySerializer.new(delivery).serialize }
  end
end
