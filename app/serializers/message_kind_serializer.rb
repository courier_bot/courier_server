# frozen_string_literal: true

# Serializer for `MessageKind` records.
class MessageKindSerializer < ApplicationSerializer
  # Serializes a message kind.
  def serialize
    {
      id: record.id,
      code: record.code,
      client: ClientSerializer.new(record.client).serialize,
      deleted: record.deleted
    }
  end
end
