# frozen_string_literal: true

# Serializer for `Delivery` records.
class DeliverySerializer < ApplicationSerializer
  # Serializes a delivery.
  def serialize
    hash = {
      external_id: record.external_id,
      delivery_kind: record.delivery_kind.code,
      delivery_fields: delivery_fields,
      status: record.status,
      created_at: record.created_at.iso8601
    }

    hash[:undelivered_reason] = record.undelivered_reason if record.undelivered?

    hash
  end

  private

  def delivery_fields
    record
      .delivery_fields
      .pluck(:name, :value)
      .to_h
      .symbolize_keys
  end
end
