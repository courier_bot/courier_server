# frozen_string_literal: true

# Base serializer class.
class ApplicationSerializer
  attr_reader :record

  # Initializes a serializer with a record.
  #
  # @param record [ActiveRecord::Base] The record to serialize.
  def initialize(record)
    @record = record
  end
end
