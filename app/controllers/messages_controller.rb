# frozen_string_literal: true

# Handles requests related to messages.
class MessagesController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /messages` requests to retrieve multiple messages.
  #
  # The response body will contain a JSON with the messages.
  def index
    messages = Message.all

    json_result = messages.map { |message| MessageSerializer.new(message).serialize }

    render json: json_result
  end

  # Handles `POST /messages` requests to schedule a message.
  #
  # This will only create a new message, and nothing else. The `PeriodicMessageDeliveriesJob` worker
  # will find this message eventually and process it.
  #
  # The request body should be a JSON containing the following attributes:
  # * `message_kind`, the message kind code, as can be found in the `MessageKind#code`;
  # * `target_messageable`, string that uniquely represents, to the client, who or what the message
  #   should be sent to; and
  # * `context`, optional, a value with which messages will be grouped.
  #
  # The response body will contain a JSON with the message data.
  def create
    message = Message.create!(message_attrs)

    render json: MessageSerializer.new(message).serialize,
           status: :created
  end

  # Handles `GET /messages/:id` requests to retrieve a message.
  #
  # The response body will contain a JSON with the message and its deliveries.
  def show
    message = Message.find(params.require(:id))

    render json: MessageSerializer.new(message).serialize
  end

  protected

  def message_attrs
    attrs = params.permit(:target_messageable, :context)
    attrs[:message_kind] = MessageKind.find_by(code: params.fetch(:message_kind) {})
    attrs[:status] = 'pending'

    attrs
  end
end
