# frozen_string_literal: true

# Handles requests related to message kinds.
class MessageKindsController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /message_kinds` requests to retrieve multiple message kinds.
  #
  # The response body will contain a JSON with the message kinds.
  def index
    message_kinds = MessageKind.all

    json_result =
      message_kinds.map { |message_kind| MessageKindSerializer.new(message_kind).serialize }

    render json: json_result
  end

  # Handles `POST /message_kinds` requests to create a message kind.
  #
  # The response body will contain a JSON with the new message kind.
  def create
    attrs = message_kind_attrs
    raise ActiveRecord::RecordNotFound, "Couldn't find Client" if message_kind_attrs[:client].blank?

    message_kind = MessageKind.create!(attrs)

    render json: MessageKindSerializer.new(message_kind).serialize,
           status: :created
  end

  # Handles `GET /message_kinds/:id` requests to retrieve a message kind.
  #
  # The response body will contain a JSON with the message kind.
  def show
    message_kind = MessageKind.find(params.require(:id))

    render json: MessageKindSerializer.new(message_kind).serialize
  end

  # Handles `PUT|PATCH /message_kinds/:id` requests to update a message kind.
  #
  # The response body will contain a JSON with the new message kind.
  def update
    message_kind = MessageKind.find(params.require(:id))
    message_kind.update!(message_kind_attrs)

    render json: MessageKindSerializer.new(message_kind).serialize
  end

  # Handles `DELETE /message_kinds/:id` requests to delete a message kind. The message kind will
  # only be soft-deleted to keep existing message's referential integrity.
  #
  # An HTTP 204 NO CONTENT will be returned.
  def destroy
    message_kind = MessageKind.find(params.require(:id))
    message_kind.update!(deleted: true)
  end

  protected

  def message_kind_attrs
    attrs = params.permit(:code, :client, :deleted)
    attrs[:client] &&= Client.find_by!(code: params.fetch(:client))

    attrs
  end
end
