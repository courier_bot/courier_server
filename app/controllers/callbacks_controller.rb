# frozen_string_literal: true

# Handles requests related to third-party service callbacks.
class CallbacksController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `POST /callbacks/:contact_kind` requests to callback following a delivery.
  #
  # Third-party services that can callback after delivering a message should be configured to make a
  # request to this route. The `contact_kind` parameter is the delivery kind's contact kind
  # attribute, with which the correct courier handler will be found.
  #
  # The callback will be processed asynchronously, so that the thread can be released as soon as
  # possible.
  def create
    CallbackRequestJob.perform_later(params.fetch(:contact_kind), params.to_unsafe_h)
  end
end
