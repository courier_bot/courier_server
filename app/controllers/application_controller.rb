# frozen_string_literal: true

# Base controller class for the application.
class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound,
              with: :http_404_not_found

  rescue_from ActiveRecord::RecordInvalid,
              with: :http_422_unprocessable_entity

  protected

  def http_404_not_found(error)
    render json: { status: '404 NOT FOUND', error: error.message },
           status: :not_found
  end

  def http_422_unprocessable_entity(error)
    render json: { status: '422 UNPROCESSABLE ENTITY', error: error.message },
           status: :unprocessable_entity
  end
end
