# frozen_string_literal: true

# Handles requests related to clients.
class ClientsController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /clients` requests to retrieve multiple clients.
  #
  # The response body will contain a JSON with the clients.
  def index
    clients = Client.all

    json_result = clients.map { |client| ClientSerializer.new(client).serialize }

    render json: json_result
  end

  # Handles `POST /clients` requests to create a client.
  #
  # The response body will contain a JSON with the new client.
  def create
    client = Client.create!(client_attrs)

    render json: ClientSerializer.new(client).serialize,
           status: :created
  end

  # Handles `GET /clients/:id` requests to retrieve a client.
  #
  # The response body will contain a JSON with the client.
  def show
    client = Client.find(params.require(:id))

    render json: ClientSerializer.new(client).serialize
  end

  # Handles `PUT|PATCH /clients/:id` requests to update a client.
  #
  # The response body will contain a JSON with the new client.
  def update
    client = Client.find(params.require(:id))
    client.update!(client_attrs)

    render json: ClientSerializer.new(client).serialize
  end

  # Handles `DELETE /clients/:id` requests to delete a client. The client will only be soft-deleted
  # to keep existing message kinds' referential integrity.
  #
  # An HTTP 204 NO CONTENT will be returned.
  def destroy
    client = Client.find(params.require(:id))
    client.update!(deleted_at: Time.zone.now)
  end

  protected

  def client_attrs
    params.permit(:code, :uri, :deleted_at)
  end
end
