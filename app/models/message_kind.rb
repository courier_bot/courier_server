# frozen_string_literal: true

# Represents a kind of message, or what happened to justify sending a message. This can be anything,
# and will determine the content of the message.
class MessageKind < ApplicationRecord
  # Code that uniquely describes a message kind. This is used to find the correct client to connect
  # with, when asking for contact information and message content.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # Which client the contact information should be requested from, as well as the message content.
  belongs_to :client

  # Whether the message kind has been deleted. Deleted message kinds are kept so that existing
  # messages keep their referential integrity.
  attribute :deleted, :boolean

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
end
