# frozen_string_literal: true

# Represents a way to contact someone or something, eg. an email or a text message.
class DeliveryKind < ApplicationRecord
  # Code that uniquely describes the delivery kind.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # Contact method so that deliveries can be grouped together even if handlers are changed. This is
  # the `contact_kind` value that courier clients will specify when requesting a message to be
  # delivered, eg. `'email'`.
  attribute :contact_kind, :string
  validates :contact_kind,
            length: { maximum: 100 },
            presence: true

  # Class name of the courier handler.
  attribute :handler_type, :string
  validates :handler_type,
            length: { maximum: 100 },
            presence: true

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
end
