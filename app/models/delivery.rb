# frozen_string_literal: true

# Represents a delivery attempt.
class Delivery < ApplicationRecord
  # Which message this delivery was for.
  belongs_to :message

  # The third-party service's id representing the delivery.
  attribute :external_id, :string
  validates :external_id,
            length: { maximum: 250 }

  # What kind of delivery should be performed.
  belongs_to :delivery_kind

  # Fields that were used by the handler to deliver the message.
  #
  # Note: The delivery field records will contain PII (personally identifiable information).
  has_many :delivery_fields, dependent: :destroy

  # Statuses that a delivery can go through. They can be the following:
  # * `pending`, when the delivery was not yet undertaken;
  # * `delivering`, when the delivery is currently being delivered;
  # * `unconfirmed`, when the delivery was sent, but no confirmations were received yet. This state
  #   should be considered as having been delivered;
  # * `delivered`, when the delivery was successfully delivered; and
  # * `undelivered`, when the delivery was unsuccessfully delivered.
  statuses = %i[pending delivering unconfirmed delivered undelivered]
  enum status: statuses.each_with_object({}) { |status, agg| agg[status] = status.to_s }
  validates :status,
            presence: true

  # When the delivery was not successful, the reason why it failed.
  attribute :undelivered_reason, :string
  validates :undelivered_reason,
            length: { maximum: 4096 }

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
end
