# frozen_string_literal: true

# Represents a message to send to someone or something.
class Message < ApplicationRecord
  # What kind of message this is.
  belongs_to :message_kind

  # Who or what this message is for. This should be a string that the client understands to uniquely
  # identify the target who should receive the message.
  attribute :target_messageable, :string
  validates :target_messageable,
            length: {
              minimum: 1,
              maximum: 250
            },
            presence: true

  # Optional context to group messages together.
  attribute :context, :string
  validates :context,
            length: { maximum: 250 }

  # Statuses that a message can go through. They can be the following:
  # * `pending`, when the message is waiting to be picked up by a background worker;
  # * `queued`, when the message is waiting to be delivered;
  # * `delivering`, when the message is in the process of being delivered;
  # * `delivered`, when the message was successfully delivered; and
  # * `undelivered`, when the message was unsuccessfully delivered.
  statuses = %i[pending queued delivering delivered undelivered]
  enum status: statuses.each_with_object({}) { |status, agg| agg[status] = status.to_s }
  validates :status,
            presence: true

  attribute :created_at, :datetime
  attribute :updated_at, :datetime

  # All attempts to deliver the message.
  has_many :deliveries, dependent: :destroy
end
