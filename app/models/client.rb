# frozen_string_literal: true

# Represents a client that uses the courier server.
class Client < ApplicationRecord
  # Code that uniquely describes a client.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # URI where the server can reach the client for more information about a specific message.
  attribute :uri, :string
  validates :uri,
            length: { maximum: 250 },
            presence: true

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
  attribute :deleted_at, :datetime
end
