# frozen_string_literal: true

# Service related to callback requests.
class CallbackRequestService
  attr_reader :callback_request,
              :delivery_kind

  # Initializes a new callback request service.
  #
  # @param callback_request [CallbackRequest] The third-party service's callback request after
  #   delivering a message.
  # @param delivery_kind [DeliveryKind] The kind of delivery that a third-party service callback was
  #   received for.
  def initialize(callback_request, delivery_kind)
    @callback_request = callback_request
    @delivery_kind = delivery_kind
  end

  # Updates the relevant records after a third-party service has called back. Doesn't do anything if
  # the callback request was to be ignored.
  def acknowledge
    return if callback_request.ignore?

    delivery = DeliveryService
               .new(delivery_kind)
               .retrieve_by_external_id(callback_request.external_id)

    message = delivery.message

    if callback_request.success
      # The handler has confirmed the delivery to be a success. At this point, the message should
      # already be set to a `delivered` status.
      delivery.delivered!
      message.delivered!

    else
      # The handler has confirmed the delivery to have failed. The message is set to `pending` to be
      # picked up by the `PeriodicMessageDeliveriesJob` job on its next pass.
      delivery.update!(
        status: 'undelivered',
        undelivered_reason: callback_request.error
      )

      message.pending!
    end
  end
end
