# frozen_string_literal: true

# Service about contact information.
class ClientService
  attr_reader :client

  # Initializes a client service with the specified `client`.
  #
  # @param client [Client] The client to connect to.
  def initialize(client)
    @client = client
  end

  # Makes a request to the client for more information about a message scheduled to be sent.
  #
  # If the client responds with an HTTP 204 NO CONTENT, the server will interpret the response as a
  # request to stop delivering the message.
  #
  # TODO: Handle unsuccessful `response.status`.
  #
  # @param message [Message] the message that is about to be delivered
  # @return [ContactInfoResponse] the client's response containing everything needed for the handler
  #   to make a request to the third-party service.
  def request_contact_info(message)
    response = Faraday.post(
      client.uri,
      { message_id: message.id }.to_json,
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    )

    return CourierHandlers::ContactInfoDeliveryEndResponse.new if response.status.equal?(204)

    hash = JSON.parse(response.body).symbolize_keys

    CourierHandlers::ContactInfoResponse
      .new(true,
           hash.delete(:contact_kind),
           hash)
  end
end
