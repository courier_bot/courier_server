# frozen_string_literal: true

# Service about delivery kinds.
class DeliveryKindService
  # Retrieves the `DeliveryKind` instance that corresponds to the specified `code`.
  #
  # @param code [String] the `DeliveryKind` code
  # @return [DeliveryKind] the `DeliveryKind` instance
  # @raise [DeliveryKindRetrievalError] if the `code` was not found in the database
  def retrieve(code)
    DeliveryKind.find_by(code: code) || invalid_delivery_kind(code)
  end

  # Retrieves the `DeliveryKind` instance that corresponds to the specified `contact_kind`.
  #
  # @param contact_kind [String] the `DeliveryKind` contact kind
  # @return [DeliveryKind] the `DeliveryKind` instance
  # @raise [DeliveryKindRetrievalError] if the `contact_kind` was not found in the database
  def retrieve_by_contact_kind(contact_kind)
    DeliveryKind.find_by(contact_kind: contact_kind) || invalid_contact_kind(contact_kind)
  end

  private

  def invalid_delivery_kind(code)
    raise DeliveryKindRetrievalError,
          "Specified delivery kind `code` attribute is invalid: `#{code}`."
  end

  def invalid_contact_kind(contact_kind)
    raise DeliveryKindRetrievalError,
          "Specified delivery kind `contact_kind` attribute is invalid: `#{contact_kind}`."
  end
end
