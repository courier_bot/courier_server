# frozen_string_literal: true

# Service about deliveries.
class DeliveryService
  attr_reader :delivery_kind

  # Initializes a new delivery service.
  #
  # @param delivery_kind [DeliveryKind] The kind of delivery that the service will be used for.
  def initialize(delivery_kind)
    @delivery_kind = delivery_kind
  end

  # Retrieves a `Delivery` by its `external_id` attribute. Different handlers may technically have
  # the same external ids. Because of this, the external ids are dependant on the delivery kind.
  #
  # @param external_id [String] The delivery's external id, provided by a third-party service.
  # @return [Delivery] The `Delivery` instance.
  # @raise [DeliveryRetrievalError] If no deliveries were found matching the specified external id.
  def retrieve_by_external_id(external_id)
    delivery = Delivery
               .find_by(delivery_kind: delivery_kind,
                        external_id: external_id)

    delivery || invalid_delivery(delivery_kind.code, external_id)
  end

  # Delivers the specified delivery instance to be processed by its handler.
  #
  # @param message [Message] the message to deliver
  # @param fields [Hash] the fields that will be given to the handler to perform the delivery
  def deliver(message, fields)
    delivery = create(message, fields)

    delivery_response = handler_service.deliver(fields)
    DeliveryResponseService.new(delivery_response, delivery).acknowledge
  end

  # Receives a callback from a third-party service related to a previous delivery.
  #
  # @param params [Hash] the body of the third-party service's callback request
  def callback(params)
    callback_request = handler_service.callback(params)
    CallbackRequestService.new(callback_request, delivery_kind).acknowledge
  end

  private

  def handler_service
    HandlerService.new(delivery_kind.handler_type)
  end

  def create(message, fields)
    delivery = message
               .deliveries
               .create!(delivery_kind_id: delivery_kind.id,
                        status: 'delivering')

    fields
      .each { |name, value| delivery.delivery_fields.create!(name: name, value: value) }

    delivery
  end

  def invalid_delivery(delivery_kind_code, external_id)
    raise DeliveryRetrievalError,
          'Specified external id is invalid for delivery kind ' \
            "`#{delivery_kind_code}`: `#{external_id}`."
  end
end
