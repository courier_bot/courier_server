# frozen_string_literal: true

# Service related to handlers. Access to handlers should be limited to methods in this service.
class HandlerService
  # The handler class.
  attr_reader :handler

  # Initializes a new handler service of the specified type.
  #
  # @param handler_type [String] The name of the `CourierHandler::BaseHandler` class.
  # @raise [HandlerTypeUnknownError] If the handler was not found in the descendant classes of
  #   `CourierHandlers::BaseHandler`. In development environments, this might also mean that the
  #   handler is not yet loaded. This would normally be done in an initializer when configuring the
  #   handlers.
  def initialize(handler_type)
    @handler = retrieve_handler_class(handler_type).new
  end

  # Makes a delivery request to the handler.
  #
  # @param fields [Hash] The necessary fields for the third-party service to perform the delivery.
  # @return [CourierHandlers::DeliveryResponse] The normalized response.
  delegate :deliver, to: :handler

  # Forwards a third-party service's callback to the handler for processing.
  #
  # The return value can also be an instance of `CourierHandlers::CallbackIgnoreRequest`, which
  # indicates that the server should ignore the callback. This is typically used for when the server
  # receives callbacks about events that it doesn't care about.
  #
  # @param params [Hash] The third-party service's request body.
  # @return [CourierHandlers::CallbackRequest] The normalized request.
  delegate :callback, to: :handler

  private

  def retrieve_handler_class(handler_type)
    CourierHandlers::BaseHandler.descendants.find do |handler|
      handler.name.eql?(handler_type)
    end || unknown_handler_type(handler_type)
  end

  def unknown_handler_type(handler_type)
    raise HandlerTypeUnknownError,
          "Handler type could not be found: `#{handler_type}`."
  end
end
