# frozen_string_literal: true

# Service related to delivery responses.
class DeliveryResponseService
  attr_reader :delivery_response,
              :delivery

  # Initializes a new delivery response service.
  #
  # @param delivery_response [DeliveryResponse] The third-party service's response to a request from
  #   the courier server to deliver a message.
  # @param delivery [Delivery] The delivery for which a request to a third-party service was
  #   performed.
  def initialize(delivery_response, delivery)
    @delivery_response = delivery_response
    @delivery = delivery
  end

  # Updates the relevant records after a third-party service has delivered a message.
  def acknowledge
    message = delivery.message
    external_id = delivery_response.external_id

    if delivery_response.success
      # The delivery is set to `'unconfirmed'` because the server only knows that the handler has
      # accepted to deliver the message. As far as it knows, the delivery was successful, but the
      # server will only be certain when it receives a confirmation during a callback.
      delivery.update!(
        external_id: external_id,
        status: 'unconfirmed'
      )

      message.delivered!
    else
      # The request to the third-party service has failed, or the third-party service has outright
      # refused to deliver the message. The message is set to `pending` to be picked up by the
      # `PeriodicMessageDeliveriesJob` job on its next pass.
      delivery.update!(
        external_id: external_id,
        status: 'undelivered',
        undelivered_reason: 'Delivery failure.'
      )

      message.pending!
    end
  end
end
