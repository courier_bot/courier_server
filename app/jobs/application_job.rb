# frozen_string_literal: true

# Base job class for the application.
class ApplicationJob < ActiveJob::Base
end
