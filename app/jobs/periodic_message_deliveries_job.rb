# frozen_string_literal: true

# Job that finds messages that are ready to be delivered.
class PeriodicMessageDeliveriesJob < ApplicationJob
  queue_as :normal

  # Tries to find pending messages, and starts a job for each of them. These messages may be new, or
  # may have been delivered multiple times already - it is the client that decides when the server
  # should stop trying to deliver a message.
  def perform
    Message
      .pending
      .each do |message|
        message.queued!
        MessageDeliveryJob.perform_later(message.id)
      end
  end
end
