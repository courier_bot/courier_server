# frozen_string_literal: true

# Job that processes a third-party service's callback request.
class CallbackRequestJob < ApplicationJob
  queue_as :normal

  # Receives a callback request and processes it.
  #
  # @param delivery_kind_code [String] the delivery kind code
  # @param params [Hash] the callback request body
  # @raise [DeliveryKindRetrievalError] if the `delivery_kind_code` was invalid
  def perform(delivery_kind_code, params)
    delivery_kind = DeliveryKindService.new.retrieve(delivery_kind_code)
    DeliveryService.new(delivery_kind).callback(params)
  end
end
