# frozen_string_literal: true

# Job that delivers a single message.
class MessageDeliveryJob < ApplicationJob
  queue_as :normal

  # Delivers a specific message. The server first makes a request to the client in charge of the
  # message kind, then makes the request to the handler for the actual delivery. As long as this job
  # is kept running in the background, these two requests will not hinder the server's main threads.
  #
  # The job is cancelled if the message is not set as `queued`. This can mean that the message did
  # not go through the `PeriodicMessageDeliveriesJob`, or that for some reason the message was
  # queued twice.
  #
  # The only way for a message to stop being delivered, except of course a successful delivery, is
  # when the client specifically requests the deliveries to end. When this happens, the message will
  # be set to an `undelivered` status, and thus will not be picked up again by the
  # `PeriodicMessageDeliveriesJob` job.
  #
  # @param message_id [Integer] the Message id
  def perform(message_id)
    message = Message.find(message_id)
    return unless message.queued?

    message.delivering!

    contact_info_response = request_contact_info(message)

    if contact_info_response.end_of_delivery?
      message.undelivered!
      return
    end

    delivery_kind = delivery_kind_from_contact_kind(contact_info_response.contact_kind)
    DeliveryService.new(delivery_kind).deliver(message, contact_info_response.fields)
  end

  private

  def request_contact_info(message)
    ClientService
      .new(message.message_kind.client)
      .request_contact_info(message)
  end

  def delivery_kind_from_contact_kind(contact_kind)
    DeliveryKindService
      .new
      .retrieve_by_contact_kind(contact_kind)
  end
end
