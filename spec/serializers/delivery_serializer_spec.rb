# frozen_string_literal: true

RSpec.describe DeliverySerializer, type: :serializer do
  let(:instance) { DeliverySerializer.new(delivery) }

  let(:delivery) do
    FactoryBot.create(
      :delivery,
      external_id: 'external_id',
      delivery_kind: FactoryBot.create(:delivery_kind, code: 'delivery_kind_code'),
      delivery_fields: [
        FactoryBot.create(:delivery_field, name: 'name_1', value: 'value_1'),
        FactoryBot.create(:delivery_field, name: 'name_2', value: 'value_2'),
        FactoryBot.create(:delivery_field, name: 'name_3', value: 'value_3')
      ],
      status: 'unconfirmed',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5)
    )
  end

  describe '#serialize' do
    let(:serializer) { instance.serialize }

    let(:expected_serialization) do
      {
        external_id: 'external_id',
        delivery_kind: 'delivery_kind_code',
        delivery_fields: {
          name_1: 'value_1',
          name_2: 'value_2',
          name_3: 'value_3'
        },
        status: 'unconfirmed',
        created_at: '2000-01-02T03:04:05Z'
      }
    end

    it 'serializes the specified delivery' do
      expect(serializer).to match(expected_serialization)
    end

    context 'when the delivery was not delivered' do
      before do
        delivery.update!(
          status: 'undelivered',
          undelivered_reason: 'failure'
        )
      end

      let(:expected_serialization) do
        {
          external_id: 'external_id',
          delivery_kind: 'delivery_kind_code',
          delivery_fields: {
            name_1: 'value_1',
            name_2: 'value_2',
            name_3: 'value_3'
          },
          status: 'undelivered',
          undelivered_reason: 'failure',
          created_at: '2000-01-02T03:04:05Z'
        }
      end

      it 'includes the undelivered_reason attribute' do
        expect(serializer).to match(expected_serialization)
      end
    end
  end
end
