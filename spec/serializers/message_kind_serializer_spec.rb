# frozen_string_literal: true

RSpec.describe MessageKindSerializer, type: :serializer do
  let(:instance) { MessageKindSerializer.new(message_kind) }

  let(:message_kind) { FactoryBot.create(:message_kind, code: 'message_kind_code', client: client) }
  let(:client) { FactoryBot.create(:client, code: 'client_code', uri: 'example.com/path') }

  describe '#serialize' do
    let(:serializer) { instance.serialize }

    let(:expected_serialization) do
      {
        id: kind_of(Integer),
        code: 'message_kind_code',
        client: ClientSerializer.new(client).serialize,
        deleted: false
      }
    end

    it 'serializes the specified message' do
      expect(serializer).to match(expected_serialization)
    end
  end
end
