# frozen_string_literal: true

RSpec.describe ClientSerializer, type: :serializer do
  let(:instance) { ClientSerializer.new(client) }

  let(:client) do
    FactoryBot.create(
      :client,
      code: 'client_code',
      uri: 'example.com/path',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
      deleted_at: nil
    )
  end

  describe '#serialize' do
    let(:serializer) { instance.serialize }

    let(:expected_serialization) do
      {
        id: kind_of(Integer),
        code: 'client_code',
        uri: 'example.com/path',
        created_at: '2000-01-02T03:04:05Z',
        updated_at: '2000-01-02T03:04:06Z',
        deleted_at: nil
      }
    end

    it 'serializes the specified message' do
      expect(serializer).to match(expected_serialization)
    end

    context 'when the `deleted_at` attribute is set' do
      before { client.update!(deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)) }

      it 'serializes the attribute' do
        expect(serializer[:deleted_at]).to eq('2000-01-02T03:04:07Z')
      end

      # An `ActiveSupport::TimeWithZone` instance is actually equal to its `#iso8601` method.
      # ```
      # t = Time.zone.local(2000, 1, 2, 3, 4, 5)
      # t == t.iso8601
      # => true
      # ```
      it 'serializes the attribute as a string' do
        expect(serializer[:deleted_at]).to be_a(String)
      end
    end
  end
end
