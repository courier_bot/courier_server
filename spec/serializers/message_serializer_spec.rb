# frozen_string_literal: true

RSpec.describe MessageSerializer, type: :serializer do
  let(:instance) { MessageSerializer.new(message) }

  let(:message) do
    FactoryBot.create(
      :message,
      message_kind: message_kind,
      target_messageable: 'target_messageable',
      context: 'context',
      status: 'delivered',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5)
    )
  end

  let(:message_kind) { FactoryBot.create(:message_kind, code: 'message_kind_code', client: client) }
  let(:client) { FactoryBot.create(:client, code: 'client_code', uri: 'example.com/path') }

  describe '#serialize' do
    let(:serializer) { instance.serialize }

    let(:expected_serialization) do
      {
        id: kind_of(Integer),
        message_kind: MessageKindSerializer.new(message_kind).serialize,
        target_messageable: 'target_messageable',
        context: 'context',
        status: 'delivered',
        created_at: '2000-01-02T03:04:05Z',
        deliveries: []
      }
    end

    it 'serializes the specified message' do
      expect(serializer).to match(expected_serialization)
    end

    context 'when the message has deliveries' do
      before do
        # Forces a reversed order in the database to test the ordering.
        second_delivery
        first_delivery
        third_delivery
      end

      let(:first_delivery) do
        FactoryBot.create(
          :delivery,
          message: message,
          created_at: Time.zone.local(2000, 1, 2, 3, 4, 5)
        )
      end

      let(:second_delivery) do
        FactoryBot.create(
          :delivery,
          message: message,
          created_at: Time.zone.local(2000, 1, 2, 3, 6, 7)
        )
      end

      let(:third_delivery) do
        FactoryBot.create(
          :delivery,
          message: message,
          created_at: Time.zone.local(2000, 1, 2, 3, 8, 9)
        )
      end

      let(:expected_serialization) do
        {
          id: kind_of(Integer),
          message_kind: MessageKindSerializer.new(message_kind).serialize,
          target_messageable: 'target_messageable',
          context: 'context',
          status: 'delivered',
          created_at: '2000-01-02T03:04:05Z',
          deliveries: [
            DeliverySerializer.new(first_delivery).serialize,
            DeliverySerializer.new(second_delivery).serialize,
            DeliverySerializer.new(third_delivery).serialize
          ]
        }
      end

      it 'serializes the specified message' do
        expect(serializer).to match(expected_serialization)
      end
    end
  end
end
