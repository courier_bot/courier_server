# frozen_string_literal: true

RSpec.describe MessagesController, type: :request do
  describe 'GET /messages' do
    subject(:request) { get '/messages' }

    before do
      FactoryBot.create(
        :message,
        message_kind: message_kind,
        target_messageable: 'target_messageable_1',
        context: 'context',
        status: 'delivered',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5)
      )

      FactoryBot.create(
        :message,
        message_kind: message_kind,
        target_messageable: 'target_messageable_2',
        context: nil,
        status: 'delivering',
        created_at: Time.zone.local(2000, 1, 2, 3, 6, 7)
      )
    end

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        client: client
      )
    end

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => kind_of(Integer),
          'message_kind' => {
            'id' => kind_of(Integer),
            'code' => 'message_kind_code',
            'client' => {
              'id' => kind_of(Integer),
              'code' => 'client_code',
              'uri' => 'example.com/path',
              'created_at' => '2000-01-02T03:04:05Z',
              'updated_at' => '2000-01-02T03:04:06Z',
              'deleted_at' => '2000-01-02T03:04:07Z'
            },
            'deleted' => false
          },
          'target_messageable' => 'target_messageable_1',
          'context' => 'context',
          'status' => 'delivered',
          'created_at' => '2000-01-02T03:04:05Z',
          'deliveries' => []
        },
        {
          'id' => kind_of(Integer),
          'message_kind' => {
            'id' => kind_of(Integer),
            'code' => 'message_kind_code',
            'client' => {
              'id' => kind_of(Integer),
              'code' => 'client_code',
              'uri' => 'example.com/path',
              'created_at' => '2000-01-02T03:04:05Z',
              'updated_at' => '2000-01-02T03:04:06Z',
              'deleted_at' => '2000-01-02T03:04:07Z'
            },
            'deleted' => false
          },
          'target_messageable' => 'target_messageable_2',
          'context' => nil,
          'status' => 'delivering',
          'created_at' => '2000-01-02T03:06:07Z',
          'deliveries' => []
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /messages' do
    subject(:request) { post '/messages', params: request_body }

    let!(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        client: client
      )
    end

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:request_body) do
      {
        message_kind: 'message_kind_code',
        target_messageable: 'target_messageable',
        context: 'context'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'message_kind' => {
          'id' => kind_of(Integer),
          'code' => 'message_kind_code',
          'client' => {
            'id' => kind_of(Integer),
            'code' => 'client_code',
            'uri' => 'example.com/path',
            'created_at' => '2000-01-02T03:04:05Z',
            'updated_at' => '2000-01-02T03:04:06Z',
            'deleted_at' => '2000-01-02T03:04:07Z'
          },
          'deleted' => false
        },
        'target_messageable' => 'target_messageable',
        'context' => 'context',
        'status' => 'pending',
        'created_at' => Time.zone.now,
        'deliveries' => []
      }
    end

    let(:expected_message_attributes) do
      {
        message_kind: message_kind,
        target_messageable: 'target_messageable',
        context: 'context',
        status: 'pending'
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a message record' do
      expect { request }.to change(Message, :count).by(1)

      request
    end

    it 'creates the message with the correct attributes' do
      request

      message = Message.last
      expect(message).to have_attributes(expected_message_attributes)
    end

    RSpec.shared_examples 'message kind error' do
      let(:expected_response) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'error' => 'Validation failed: Message kind must exist'
        }
      end

      it 'responds with an HTTP 422 UNPROCESSABLE ENTITY' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end

    context 'when the specified message kind is nil' do
      before { request_body[:message_kind] = nil }

      include_examples 'message kind error'
    end

    context 'when the specified message kind is empty' do
      before { request_body[:message_kind] = '  ' }

      include_examples 'message kind error'
    end

    context 'when the specified message kind is invalid' do
      before { request_body[:message_kind] = 'invalid' }

      include_examples 'message kind error'
    end

    context 'when the message kind is not specified' do
      before { request_body.delete(:message_kind) }

      include_examples 'message kind error'
    end

    context 'when the context is not specified' do
      before { request_body.delete(:context) }

      it 'still creates a message record' do
        expect { request }.to change(Message, :count).by(1)

        request
      end

      it 'creates the message with no context' do
        request

        message = Message.last
        expect(message).to have_attributes(context: nil)
      end
    end
  end

  describe 'GET /messages/:id' do
    subject(:request) { get "/messages/#{requested_message_id}" }

    let(:requested_message_id) { message.id }

    let(:message) do
      message = FactoryBot.create(
        :message,
        message_kind: message_kind,
        target_messageable: 'target_messageable',
        context: 'context',
        status: 'delivered',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5)
      )

      message.deliveries << FactoryBot.create(
        :delivery,
        external_id: 'external_id_1',
        delivery_kind: FactoryBot.create(:delivery_kind, code: 'delivery_kind_code_1'),
        delivery_fields: [
          FactoryBot.create(:delivery_field, name: 'name_1', value: 'value_1'),
          FactoryBot.create(:delivery_field, name: 'name_2', value: 'value_2'),
          FactoryBot.create(:delivery_field, name: 'name_3', value: 'value_3')
        ],
        status: 'unconfirmed',
        created_at: Time.zone.local(2000, 1, 2, 3, 6, 7)
      )

      message.deliveries << FactoryBot.create(
        :delivery,
        external_id: 'external_id_2',
        delivery_kind: FactoryBot.create(:delivery_kind, code: 'delivery_kind_code_2'),
        delivery_fields: [
          FactoryBot.create(:delivery_field, name: 'name_4', value: 'value_4'),
          FactoryBot.create(:delivery_field, name: 'name_5', value: 'value_5')
        ],
        status: 'undelivered',
        undelivered_reason: 'failure',
        created_at: Time.zone.local(2000, 1, 2, 3, 8, 9)
      )

      message
    end

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        client: client
      )
    end

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'message_kind' => {
          'id' => kind_of(Integer),
          'code' => 'message_kind_code',
          'client' => {
            'id' => kind_of(Integer),
            'code' => 'client_code',
            'uri' => 'example.com/path',
            'created_at' => '2000-01-02T03:04:05Z',
            'updated_at' => '2000-01-02T03:04:06Z',
            'deleted_at' => '2000-01-02T03:04:07Z'
          },
          'deleted' => false
        },
        'target_messageable' => 'target_messageable',
        'context' => 'context',
        'status' => 'delivered',
        'created_at' => '2000-01-02T03:04:05Z',
        'deliveries' => [
          {
            'external_id' => 'external_id_1',
            'delivery_kind' => 'delivery_kind_code_1',
            'delivery_fields' => {
              'name_1' => 'value_1',
              'name_2' => 'value_2',
              'name_3' => 'value_3'
            },
            'status' => 'unconfirmed',
            'created_at' => '2000-01-02T03:06:07Z'
          },
          {
            'external_id' => 'external_id_2',
            'delivery_kind' => 'delivery_kind_code_2',
            'delivery_fields' => {
              'name_4' => 'value_4',
              'name_5' => 'value_5'
            },
            'status' => 'undelivered',
            'undelivered_reason' => 'failure',
            'created_at' => '2000-01-02T03:08:09Z'
          }
        ]
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    context 'when the requested message id is not found' do
      let(:requested_message_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find Message with 'id'=-1"
        }
      end

      it 'responds with an HTTP 400 NOT FOUND' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end
end
