# frozen_string_literal: true

RSpec.describe ClientsController, type: :request do
  describe 'GET /clients' do
    subject(:request) { get '/clients' }

    before do
      FactoryBot.create(
        :client,
        code: 'client_code_1',
        uri: 'example.com/path_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )

      FactoryBot.create(
        :client,
        code: 'client_code_2',
        uri: 'example.com/path_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => kind_of(Integer),
          'code' => 'client_code_1',
          'uri' => 'example.com/path_1',
          'created_at' => '2000-01-02T03:04:05Z',
          'updated_at' => '2000-01-02T03:04:06Z',
          'deleted_at' => '2000-01-02T03:04:07Z'
        },
        {
          'id' => kind_of(Integer),
          'code' => 'client_code_2',
          'uri' => 'example.com/path_2',
          'created_at' => '2000-01-02T03:04:08Z',
          'updated_at' => '2000-01-02T03:04:09Z',
          'deleted_at' => '2000-01-02T03:04:10Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /clients', :freeze_time do
    subject(:request) { post '/clients', params: request_body }

    let(:request_body) do
      {
        code: 'client_code',
        uri: 'example.com/path'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'code' => 'client_code',
        'uri' => 'example.com/path',
        'created_at' => Time.zone.now.iso8601,
        'updated_at' => Time.zone.now.iso8601,
        'deleted_at' => nil
      }
    end

    let(:expected_client_attributes) do
      {
        code: 'client_code',
        uri: 'example.com/path',
        'created_at' => Time.zone.now,
        'updated_at' => Time.zone.now,
        'deleted_at' => nil
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a client record' do
      expect { request }.to change(Client, :count).by(1)

      request
    end

    it 'creates the client with the correct attributes' do
      request

      client = Client.last
      expect(client).to have_attributes(expected_client_attributes)
    end
  end

  describe 'GET /clients/:id' do
    subject(:request) { get "/clients/#{requested_client_id}" }

    let(:requested_client_id) { client.id }

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'code' => 'client_code',
        'uri' => 'example.com/path',
        'created_at' => '2000-01-02T03:04:05Z',
        'updated_at' => '2000-01-02T03:04:06Z',
        'deleted_at' => '2000-01-02T03:04:07Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    context 'when the requested client id is not found' do
      let(:requested_client_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find Client with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end

  describe 'PUT|PATCH /clients/:id', :freeze_time do
    subject(:request) { put "/clients/#{requested_client_id}", params: request_body }

    let(:requested_client_id) { client.id }

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:request_body) do
      {
        code: 'updated_client_code',
        uri: 'example.com/updated_path'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'code' => 'updated_client_code',
        'uri' => 'example.com/updated_path',
        'created_at' => '2000-01-02T03:04:05Z',
        'updated_at' => Time.zone.now.iso8601,
        'deleted_at' => '2000-01-02T03:04:07Z'
      }
    end

    let(:expected_client_attributes) do
      {
        code: 'updated_client_code',
        uri: 'example.com/updated_path'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the client with the correct attributes' do
      request

      client.reload
      expect(client).to have_attributes(expected_client_attributes)
    end

    context 'when the requested client id is not found' do
      let(:requested_client_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find Client with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end

  describe 'DELETE /clients/:id', :freeze_time do
    subject(:request) { delete "/clients/#{requested_client_id}" }

    let(:requested_client_id) { client.id }

    let(:client) { FactoryBot.create(:client) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'updates the client `deleted_at` attribute to the current time' do
      request

      client.reload
      expect(client).to have_attributes(deleted_at: Time.zone.now)
    end

    context 'when the requested client id is not found' do
      let(:requested_client_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find Client with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end
end
