# frozen_string_literal: true

RSpec.describe CallbacksController, type: :request do
  describe 'POST /callbacks/:delivery_kind' do
    subject(:request) do
      post '/callbacks/delivery_kind',
           params: { arg: 'arbitrary argument' }
    end

    before do
      allow(DeliveryService).to receive(:new).and_return(delivery_service)
      allow(delivery_service).to receive(:callback)
    end

    let(:delivery_service) { instance_spy(DeliveryService) }

    it 'responds with an HTTP 204 and an empty body' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'responds with an empty body' do
      request

      expect(response.body).to be_empty
    end

    it 'does not process the callback immediately' do
      request

      expect(delivery_service).not_to have_received(:callback)
    end

    it 'queues the callback request for further processing' do
      ActiveJob::Base.queue_adapter = :test

      request

      expect(CallbackRequestJob).to have_been_enqueued
        .on_queue('normal').at(:no_wait)
        .with('delivery_kind', hash_including(arg: 'arbitrary argument')).once
    end
  end
end
