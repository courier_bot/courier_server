# frozen_string_literal: true

RSpec.describe MessageKindsController, type: :request do
  RSpec.shared_examples 'client error' do
    let(:expected_response) do
      {
        'status' => '404 NOT FOUND',
        'error' => "Couldn't find Client"
      }
    end

    it 'responds with an HTTP 404 NOT FOUND' do
      request

      expect(response).to have_http_status(:not_found)
    end

    it 'responds with an error response' do
      request

      expect(JSON.parse(response.body)).to match(expected_response)
    end
  end

  describe 'GET /message_kinds' do
    subject(:request) { get '/message_kinds' }

    before do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code_1',
        client: client
      )

      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code_2',
        client: client
      )
    end

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => kind_of(Integer),
          'code' => 'message_kind_code_1',
          'client' => {
            'id' => kind_of(Integer),
            'code' => 'client_code',
            'uri' => 'example.com/path',
            'created_at' => '2000-01-02T03:04:05Z',
            'updated_at' => '2000-01-02T03:04:06Z',
            'deleted_at' => '2000-01-02T03:04:07Z'
          },
          'deleted' => false
        },
        {
          'id' => kind_of(Integer),
          'code' => 'message_kind_code_2',
          'client' => {
            'id' => kind_of(Integer),
            'code' => 'client_code',
            'uri' => 'example.com/path',
            'created_at' => '2000-01-02T03:04:05Z',
            'updated_at' => '2000-01-02T03:04:06Z',
            'deleted_at' => '2000-01-02T03:04:07Z'
          },
          'deleted' => false
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /message_kinds' do
    subject(:request) { post '/message_kinds', params: request_body }

    let!(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:request_body) do
      {
        code: 'message_kind_code',
        client: 'client_code'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'code' => 'message_kind_code',
        'client' => {
          'id' => kind_of(Integer),
          'code' => 'client_code',
          'uri' => 'example.com/path',
          'created_at' => '2000-01-02T03:04:05Z',
          'updated_at' => '2000-01-02T03:04:06Z',
          'deleted_at' => '2000-01-02T03:04:07Z'
        },
        'deleted' => false
      }
    end

    let(:expected_message_kind_attributes) do
      {
        code: 'message_kind_code',
        client: client
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a message kind record' do
      expect { request }.to change(MessageKind, :count).by(1)

      request
    end

    it 'creates the message kind with the correct attributes' do
      request

      message_kind = MessageKind.last
      expect(message_kind).to have_attributes(expected_message_kind_attributes)
    end

    context 'when the specified client is nil' do
      before { request_body[:client] = nil }

      include_examples 'client error'
    end

    context 'when the specified client is empty' do
      before { request_body[:client] = '  ' }

      include_examples 'client error'
    end

    context 'when the specified client is invalid' do
      before { request_body[:client] = 'invalid' }

      include_examples 'client error'
    end

    context 'when the client is not specified' do
      before { request_body.delete(:client) }

      include_examples 'client error'
    end
  end

  describe 'GET /message_kinds/:id' do
    subject(:request) { get "/message_kinds/#{requested_message_kind_id}" }

    let(:requested_message_kind_id) { message_kind.id }

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        client: client
      )
    end

    let(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(Integer),
        'code' => 'message_kind_code',
        'client' => {
          'id' => kind_of(Integer),
          'code' => 'client_code',
          'uri' => 'example.com/path',
          'created_at' => '2000-01-02T03:04:05Z',
          'updated_at' => '2000-01-02T03:04:06Z',
          'deleted_at' => '2000-01-02T03:04:07Z'
        },
        'deleted' => false
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    context 'when the requested message kind id is not found' do
      let(:requested_message_kind_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find MessageKind with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end

  describe 'PUT|PATCH /message_kinds/:id' do
    subject(:request) { put "/message_kinds/#{requested_message_kind_id}", params: request_body }

    let(:requested_message_kind_id) { message_kind.id }

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        client: client
      )
    end

    let!(:client) do
      FactoryBot.create(
        :client,
        code: 'client_code',
        uri: 'example.com/path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let!(:other_client) do
      FactoryBot.create(
        :client,
        code: 'other_client_code',
        uri: 'example.com/other_path',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:request_body) do
      {
        code: 'updated_message_kind_code',
        client: 'other_client_code',
        deleted: true
      }
    end

    let(:expected_response_body) do
      {
        'id' => message_kind.id,
        'code' => 'updated_message_kind_code',
        'client' => {
          'id' => other_client.id,
          'code' => 'other_client_code',
          'uri' => 'example.com/other_path',
          'created_at' => '2000-01-02T03:04:08Z',
          'updated_at' => '2000-01-02T03:04:09Z',
          'deleted_at' => '2000-01-02T03:04:10Z'
        },
        'deleted' => true
      }
    end

    let(:expected_message_kind_attributes) do
      {
        code: 'updated_message_kind_code',
        client: other_client,
        deleted: true
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the message kind with the correct attributes' do
      request

      message_kind.reload
      expect(message_kind).to have_attributes(expected_message_kind_attributes)
    end

    context 'when the requested message kind id is not found' do
      let(:requested_message_kind_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find MessageKind with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end

    context 'when the specified client is empty' do
      before { request_body[:client] = '  ' }

      include_examples 'client error'
    end

    context 'when the specified client is invalid' do
      before { request_body[:client] = 'invalid' }

      include_examples 'client error'
    end

    context 'when only the `code` attribute is updated' do
      let(:request_body) do
        {
          code: 'updated_message_kind_code'
        }
      end

      it 'updates the `code` attribute' do
        request

        message_kind.reload
        expect(message_kind).to have_attributes(code: 'updated_message_kind_code')
      end

      it 'does not update the `client` attribute' do
        request

        message_kind.reload
        expect(message_kind).to have_attributes(client: client)
      end
    end

    context 'when only the `client` attribute is updated' do
      let(:request_body) do
        {
          client: 'other_client_code'
        }
      end

      it 'updates the `client` attribute' do
        request

        message_kind.reload
        expect(message_kind).to have_attributes(client: other_client)
      end

      it 'does not update the `code` attribute' do
        request

        message_kind.reload
        expect(message_kind).to have_attributes(code: 'message_kind_code')
      end
    end
  end

  describe 'DELETE /message_kinds/:id' do
    subject(:request) { delete "/message_kinds/#{requested_message_kind_id}" }

    let(:requested_message_kind_id) { message_kind.id }

    let(:message_kind) { FactoryBot.create(:message_kind) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'updates the message kind `deleted` attribute to `true`' do
      request

      message_kind.reload
      expect(message_kind).to have_attributes(deleted: true)
    end

    context 'when the requested message kind id is not found' do
      let(:requested_message_kind_id) { -1 }

      let(:expected_response) do
        {
          'status' => '404 NOT FOUND',
          'error' => "Couldn't find MessageKind with 'id'=-1"
        }
      end

      it 'responds with an HTTP 404 BAD REQUEST' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with an error response' do
        request

        expect(JSON.parse(response.body)).to match(expected_response)
      end
    end
  end
end
