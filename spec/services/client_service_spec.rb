# frozen_string_literal: true

RSpec.describe ClientService, type: :service do
  let(:instance) { ClientService.new(client) }

  let(:client) { FactoryBot.create(:client) }

  describe '#request_contact_info' do
    subject(:service) { instance.request_contact_info(message) }

    before do
      stub_request(:post, client.uri)
        .with(headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' },
              body: "{\"message_id\":#{message.id}}")
        .to_return(client_response)
    end

    let(:message) { FactoryBot.create(:message, message_kind: message_kind) }

    let(:message_kind) { FactoryBot.create(:message_kind, client: client) }

    let(:client_response) do
      {
        body: '{"contact_kind":"contact_kind",' \
                '"to":"to_user",' \
                '"content":"content"}',
        status: 200
      }
    end

    it 'returns a `ContactInfoResponse` instance' do
      expect(service).to be_a(CourierHandlers::ContactInfoResponse)
    end

    it 'returns a contact info response with no end of deliveries' do
      expect(service).not_to be_end_of_delivery
    end

    it 'returns the normalized contact information' do
      expect(service).to have_attributes(
        success: true,
        contact_kind: 'contact_kind',
        fields: { to: 'to_user', content: 'content' }
      )
    end

    context 'when the client responds with an HTTP 204 NO CONTENT' do
      let(:client_response) { { status: 204 } }

      it 'returns a `ContactInfoDeliveryEndResponse` instance' do
        expect(service).to be_a(CourierHandlers::ContactInfoDeliveryEndResponse)
      end

      it 'returns a contact info response with an end of delivery' do
        expect(service).to be_end_of_delivery
      end
    end
  end
end
