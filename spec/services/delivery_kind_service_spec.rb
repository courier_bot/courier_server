# frozen_string_literal: true

RSpec.describe DeliveryKindService, type: :service do
  let(:instance) { DeliveryKindService.new }

  describe '#retrieve' do
    subject(:service) { instance.retrieve(code) }

    let(:code) { 'delivery_kind_code' }

    let!(:delivery_kind) { FactoryBot.create(:delivery_kind, code: 'delivery_kind_code') }

    it 'returns the `DeliveryKind` instance' do
      expect(service).to eq(delivery_kind)
    end

    context 'when the specified delivery kind code is invalid' do
      let(:code) { 'invalid' }

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryKindRetrievalError,
          'Specified delivery kind `code` attribute is invalid: `invalid`.'
        )
      end
    end
  end

  describe '#retrieve_by_contact_kind' do
    subject(:service) { instance.retrieve_by_contact_kind(contact_kind) }

    let(:contact_kind) { 'contact_kind' }

    let!(:delivery_kind) { FactoryBot.create(:delivery_kind, contact_kind: 'contact_kind') }

    it 'returns the `DeliveryKind` instance' do
      expect(service).to eq(delivery_kind)
    end

    context 'when the specified contact kind is invalid' do
      let(:contact_kind) { 'invalid' }

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryKindRetrievalError,
          'Specified delivery kind `contact_kind` attribute is invalid: `invalid`.'
        )
      end
    end
  end
end
