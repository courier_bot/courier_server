# frozen_string_literal: true

RSpec.describe DeliveryService, type: :service do
  before do
    allow(HandlerService).to receive(:new).and_return(handler_service)
  end

  let(:instance) { DeliveryService.new(delivery_kind) }

  let(:delivery_kind) { FactoryBot.create(:delivery_kind, code: 'delivery_kind') }

  let(:handler_service) { instance_spy(HandlerService) }

  describe '#retrieve_by_external_id' do
    subject(:service) { instance.retrieve_by_external_id(external_id) }

    let(:external_id) { 'external_id' }

    let!(:delivery) do
      FactoryBot.create(:delivery, delivery_kind: actual_delivery_kind, external_id: 'external_id')
    end

    let(:actual_delivery_kind) { delivery_kind }

    it 'returns the delivery' do
      expect(service).to eq(delivery)
    end

    context 'when the specified external id does not exist' do
      let(:external_id) { 'invalid' }

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryRetrievalError,
          'Specified external id is invalid for delivery kind `delivery_kind`: `invalid`.'
        )
      end
    end

    context 'when the specified external id is related to another delivery kind' do
      let(:actual_delivery_kind) { FactoryBot.create(:delivery_kind, code: 'other_delivery_kind') }

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryRetrievalError,
          'Specified external id is invalid for delivery kind `delivery_kind`: `external_id`.'
        )
      end
    end
  end

  describe '#deliver' do
    subject(:service) { instance.deliver(message, fields) }

    before do
      allow(handler_service).to receive(:deliver).and_return(delivery_response)

      allow(DeliveryResponseService).to receive(:new).and_return(delivery_response_service)
      allow(delivery_response_service).to receive(:acknowledge)
    end

    let(:message) { FactoryBot.create(:message, status: 'delivering') }
    let(:fields) do
      {
        to: 'to_user',
        arg: 'field'
      }
    end

    let(:delivery_response) { instance_double(Hash) }

    let(:delivery_response_service) { instance_spy(DeliveryResponseService) }

    it 'creates a handler service instance with the correct type' do
      service

      expect(HandlerService).to have_received(:new).with(delivery_kind.handler_type).once
    end

    it 'creates a new delivery' do
      expect { service }.to change { message.deliveries.count }.by(1)
    end

    it 'creates the delivery with the correct attributes' do
      service

      delivery = message.reload.deliveries.first
      expect(delivery).to have_attributes(delivery_kind_id: delivery_kind.id, status: 'delivering')
    end

    it 'creates a delivery field for each fields' do
      service

      delivery = message.reload.deliveries.first
      expect(delivery.delivery_fields.count).to eq(2)
    end

    it 'creates the delivery field with the correct attributes' do
      expected_delivery_field_values = [%w[to to_user], %w[arg field]]

      service

      delivery_field_values = message.reload.deliveries.first.delivery_fields.pluck(:name, :value)
      expect(delivery_field_values).to match_array(expected_delivery_field_values)
    end

    it 'delivers using the handler service' do
      service

      expect(handler_service).to have_received(:deliver)
        .with(to: 'to_user', arg: 'field').once
    end

    it 'initializes the delivery response service with the correct parameters' do
      service

      expect(DeliveryResponseService).to have_received(:new)
        .with(delivery_response, kind_of(Delivery)).once
    end

    it 'processes the response using the delivery response service' do
      service

      expect(delivery_response_service).to have_received(:acknowledge).once
    end
  end

  describe '#callback' do
    subject(:service) { instance.callback(params) }

    before do
      allow(handler_service).to receive(:callback).and_return(callback_request)

      allow(CallbackRequestService).to receive(:new).and_return(callback_request_service)
      allow(callback_request_service).to receive(:acknowledge)
    end

    let(:params) { instance_double(Hash) }

    let(:callback_request) { instance_double(Hash) }

    let(:callback_request_service) { instance_spy(CallbackRequestService) }

    it 'creates a handler service instance with the correct type' do
      service

      expect(HandlerService).to have_received(:new).with(delivery_kind.handler_type).once
    end

    it 'callbacks using the handler service' do
      service

      expect(handler_service).to have_received(:callback).with(params).once
    end

    it 'initializes the callback request service with the correct parameters' do
      service

      expect(CallbackRequestService).to have_received(:new)
        .with(callback_request, delivery_kind).once
    end

    it 'processes the request using the callback request service' do
      service

      expect(callback_request_service).to have_received(:acknowledge).once
    end
  end
end
