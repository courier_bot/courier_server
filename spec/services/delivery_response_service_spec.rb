# frozen_string_literal: true

RSpec.describe DeliveryResponseService, type: :service do
  let(:instance) { DeliveryResponseService.new(delivery_response, delivery) }

  let(:delivery) { FactoryBot.create(:delivery, message: message) }

  let(:message) { FactoryBot.create(:message, status: 'delivering') }

  let(:delivery_response) do
    CourierHandlers::DeliveryResponse.new(
      handler_delivery_success,
      'external_id'
    )
  end

  let(:handler_delivery_success) { true }

  describe '#acknowledge' do
    subject(:service) { instance.acknowledge }

    let(:expected_delivery_attributes) do
      {
        status: 'unconfirmed',
        external_id: 'external_id'
      }
    end

    it 'updates the delivery with the correct attributes', :freeze_time do
      service

      expect(delivery.reload).to have_attributes(expected_delivery_attributes)
    end

    it 'sets the message as being delivered' do
      service

      expect(delivery.reload.message).to be_delivered
    end

    context 'when the delivery has failed' do
      let(:handler_delivery_success) { false }

      let(:expected_delivery_attributes) do
        {
          status: 'undelivered',
          external_id: 'external_id',
          undelivered_reason: 'Delivery failure.'
        }
      end

      it 'updates the delivery with the correct attributes' do
        service

        expect(delivery.reload).to have_attributes(expected_delivery_attributes)
      end

      it 'sends the message back to the queue' do
        service

        expect(delivery.reload.message).to be_pending
      end
    end
  end
end
