# frozen_string_literal: true

RSpec.describe CallbackRequestService, type: :service do
  let(:instance) { CallbackRequestService.new(callback_request, delivery_kind) }

  let(:delivery_kind) { FactoryBot.create(:delivery_kind, code: 'delivery_kind') }

  let(:callback_request) do
    CourierHandlers::CallbackRequest.new(
      callback_success,
      external_id
    )
  end

  let(:callback_success) { true }

  let(:external_id) { 'external_id' }

  describe '#acknowledge' do
    subject(:service) { instance.acknowledge }

    let(:message) { FactoryBot.create(:message, status: 'delivering') }

    let!(:delivery) do
      FactoryBot.create(
        :delivery,
        message: message,
        delivery_kind: delivery_kind,
        external_id: 'external_id',
        status: 'unconfirmed'
      )
    end

    it 'sets the delivery as being delivered' do
      service

      expect(delivery.reload).to be_delivered
    end

    it 'sets the message as being delivered' do
      service

      expect(message.reload).to be_delivered
    end

    context 'when the callback was not successful' do
      before { callback_request.error = 'handler error' }

      let(:callback_success) { false }

      it 'sets the delivery as being undelivered' do
        service

        expect(delivery.reload).to have_attributes(
          status: 'undelivered',
          undelivered_reason: 'handler error'
        )
      end

      it 'sets the message as being pending' do
        service

        expect(message.reload).to be_pending
      end
    end

    context 'when the callback is to be ignored' do
      let(:callback_request) { CourierHandlers::CallbackIgnoreRequest.new }

      it 'ignores the callback' do
        service

        expect(delivery.reload).to be_unconfirmed
      end
    end

    context 'when the external id is not in the database' do
      let(:external_id) { 'other_external_id' }

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryRetrievalError,
          'Specified external id is invalid for delivery kind `delivery_kind`: `other_external_id`.'
        )
      end
    end

    context 'when the external id is related to another delivery kind' do
      let(:instance) { CallbackRequestService.new(callback_request, other_delivery_kind) }

      let(:other_delivery_kind) do
        FactoryBot.create(:delivery_kind, code: 'other_delivery_kind')
      end

      it 'raises an error' do
        expect { service }.to raise_error(
          DeliveryRetrievalError,
          'Specified external id is invalid for delivery kind `other_delivery_kind`: `external_id`.'
        )
      end
    end
  end
end
