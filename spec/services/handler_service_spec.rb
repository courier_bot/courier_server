# frozen_string_literal: true

RSpec.describe HandlerService, type: :service do
  # Test handler for method expectations.
  class HandlerServiceMockHandler < CourierHandlers::BaseHandler
    def deliver(_fields)
      :delivery_response
    end

    def callback(_args)
      :callback_request
    end
  end

  let(:instance) { HandlerService.new(HandlerServiceMockHandler.name) }

  describe '.initialize' do
    subject(:instance) { HandlerService.new(handler_type) }

    let(:handler_type) { HandlerServiceMockHandler.name }

    it 'sets the `handler` instance variable' do
      instance

      expect(instance.instance_variable_get(:@handler)).to be_a(HandlerServiceMockHandler)
    end

    context 'when the handler type is not found' do
      let(:handler_type) { 'UnknownClass' }

      it 'raises a `HandlerTypeUnknownError` error' do
        expect { instance }.to raise_error(
          HandlerTypeUnknownError,
          'Handler type could not be found: `UnknownClass`.'
        )
      end
    end
  end

  describe '#deliver' do
    subject(:service) { instance.deliver(fields) }

    let(:fields) { instance_double(Hash) }

    it 'forwards the contact info to the handler' do
      expect(service).to eq(:delivery_response)
    end
  end

  describe '#callback' do
    subject(:service) { instance.callback(params) }

    let(:params) { instance_double(Hash) }

    it 'forwards the params to the handler' do
      expect(service).to eq(:callback_request)
    end
  end
end
