# frozen_string_literal: true

RSpec.describe MessageDeliveryJob, type: :job do
  describe '.perform_now' do
    subject(:job) { MessageDeliveryJob.perform_now(message_id) }

    before do
      allow(ClientService).to receive(:new).and_return(client_service)
      allow(client_service).to receive(:request_contact_info).and_return(contact_info_response)
      allow(DeliveryService).to receive(:new).and_return(delivery_service)
      allow(delivery_service).to receive(:deliver)
    end

    let(:client_service)   { instance_spy(ClientService) }

    let(:delivery_service) { instance_spy(DeliveryService) }

    let(:contact_info_response) do
      CourierHandlers::ContactInfoResponse
        .new(true,
             'contact_kind',
             field1: 'first field',
             field2: 'second field')
    end

    let(:message) { FactoryBot.create(:message, status: message_status) }

    let(:message_status) { 'queued' }

    let(:message_id) { message.id }

    let!(:delivery_kind) do
      FactoryBot.create(:delivery_kind, contact_kind: 'contact_kind')
    end

    it 'initializes the client service with the correct client' do
      job

      expect(ClientService).to have_received(:new).with(message.message_kind.client).once
    end

    it 'requests the contact info' do
      job

      expect(client_service).to have_received(:request_contact_info).with(message).once
    end

    it 'initializes the delivery service with the correct delivery kind' do
      job

      expect(DeliveryService).to have_received(:new).with(delivery_kind).once
    end

    it 'sets the message with a status of `delivering`' do
      job

      expect(message.reload).to be_delivering
    end

    it 'delivers the message' do
      job

      expect(delivery_service).to have_received(:deliver)
        .with(kind_of(Message), field1: 'first field', field2: 'second field').once
    end

    context 'when the message is not set as being `queued`' do
      let(:message_status) { 'pending' }

      it 'does not deliver the message' do
        job

        expect(delivery_service).not_to have_received(:deliver)
      end
    end

    context 'when the client wants to stop delivering the message' do
      let(:contact_info_response) do
        CourierHandlers::ContactInfoDeliveryEndResponse.new
      end

      it 'does not delivery the message' do
        job

        expect(delivery_service).not_to have_received(:deliver)
      end

      it 'sets the message as being undelivered' do
        job

        expect(message.reload).to be_undelivered
      end
    end
  end
end
