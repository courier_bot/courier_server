# frozen_string_literal: true

RSpec.describe PeriodicMessageDeliveriesJob, type: :job do
  describe '.perform_now' do
    subject(:job) { PeriodicMessageDeliveriesJob.perform_now }

    let!(:pending_message)     { FactoryBot.create(:message, status: 'pending')     }
    let!(:queued_message)      { FactoryBot.create(:message, status: 'queued')      }
    let!(:delivering_message)  { FactoryBot.create(:message, status: 'delivering')  }
    let!(:delivered_message)   { FactoryBot.create(:message, status: 'delivered')   }
    let!(:undelivered_message) { FactoryBot.create(:message, status: 'undelivered') }

    it "updates pending messages' status to `queued`" do
      job

      expect(pending_message.reload).to be_queued
    end

    it "does not update delivered messages' status to `delivering`" do
      job

      expect(delivered_message.reload).not_to be_delivering
    end

    it "does not update undelivered messages' status to `delivering`" do
      job

      expect(undelivered_message.reload).not_to be_delivering
    end

    it 'triggers a job to deliver messages with status `pending`' do
      ActiveJob::Base.queue_adapter = :test

      job

      expect(MessageDeliveryJob).to have_been_enqueued
        .on_queue('normal').at(:no_wait).with(pending_message.id).once
    end

    it 'does not trigger a job for messages with status `queued`' do
      ActiveJob::Base.queue_adapter = :test

      job

      expect(MessageDeliveryJob).not_to have_been_enqueued.with(queued_message.id)
    end

    it 'does not trigger a job for messages with status `delivering`' do
      ActiveJob::Base.queue_adapter = :test

      job

      expect(MessageDeliveryJob).not_to have_been_enqueued.with(delivering_message.id)
    end

    it 'does not trigger a job for messages with status `delivered`' do
      ActiveJob::Base.queue_adapter = :test

      job

      expect(MessageDeliveryJob).not_to have_been_enqueued.with(delivered_message.id)
    end

    it 'does not trigger a job for messages with status `undelivered`' do
      ActiveJob::Base.queue_adapter = :test

      job

      expect(MessageDeliveryJob).not_to have_been_enqueued.with(undelivered_message.id)
    end
  end
end
