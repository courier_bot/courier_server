# frozen_string_literal: true

RSpec.describe CallbackRequestJob, type: :job do
  describe '.perform_now' do
    subject(:job) { CallbackRequestJob.perform_now(delivery_kind_code, params) }

    before do
      allow(DeliveryService).to receive(:new).and_return(delivery_service)
      allow(delivery_service).to receive(:callback)
    end

    let(:delivery_service) { instance_spy(DeliveryService) }

    let(:delivery_kind) { FactoryBot.create(:delivery_kind, code: 'delivery_kind') }

    let(:delivery_kind_code) { delivery_kind.code }

    let(:params) { instance_double(Hash) }

    it 'initializes the delivery service with the correct delivery kind' do
      job

      expect(DeliveryService).to have_received(:new).with(delivery_kind).once
    end

    it 'forwards the callback request to the delivery service' do
      job

      expect(delivery_service).to have_received(:callback).with(params).once
    end

    context 'when the delivery kind code is not found' do
      let(:delivery_kind_code) { 'invalid' }

      it 'raises a `DeliveryKindRetrievalError` error' do
        expect { job }.to raise_error(
          DeliveryKindRetrievalError,
          'Specified delivery kind `code` attribute is invalid: `invalid`.'
        )
      end
    end
  end
end
