# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    message_kind
    target_messageable { Faker::String.random.tr("\u0000", '') }
    status { 'delivered' }
  end
end
