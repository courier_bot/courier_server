# frozen_string_literal: true

FactoryBot.define do
  factory :client do
    code { Faker::String.random.tr("\u0000", '') }
    uri { Faker::Internet.url }
    deleted_at { nil }
  end
end
