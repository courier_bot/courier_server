# frozen_string_literal: true

FactoryBot.define do
  factory :delivery_field do
    name { Faker::String.random.tr("\u0000", '') }
    value { Faker::String.random.tr("\u0000", '') }
  end
end
