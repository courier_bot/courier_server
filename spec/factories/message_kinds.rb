# frozen_string_literal: true

FactoryBot.define do
  factory :message_kind do
    code { Faker::String.random.tr("\u0000", '') }
    client
    deleted { false }
  end
end
