# frozen_string_literal: true

FactoryBot.define do
  factory :delivery do
    message
    delivery_kind
    status { 'pending' }
  end
end
