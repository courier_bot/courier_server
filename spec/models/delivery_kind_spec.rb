# frozen_string_literal: true

RSpec.describe DeliveryKind, type: :model do
  subject { FactoryBot.create(:delivery_kind) }

  describe 'attributes' do
    describe '#code' do
      it { is_expected.to validate_length_of(:code).is_at_most(100) }
      it { is_expected.to validate_uniqueness_of(:code) }
      it { is_expected.to validate_presence_of(:code) }
    end

    describe '#contact_kind' do
      it { is_expected.to validate_length_of(:contact_kind).is_at_most(100) }
      it { is_expected.to validate_presence_of(:contact_kind) }
    end

    describe '#handler_type' do
      it { is_expected.to validate_length_of(:handler_type).is_at_most(100) }
      it { is_expected.to validate_presence_of(:handler_type) }
    end
  end
end
