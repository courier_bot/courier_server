# frozen_string_literal: true

RSpec.describe Client, type: :model do
  subject { FactoryBot.create(:client) }

  describe 'attributes' do
    describe '#code' do
      it { is_expected.to validate_length_of(:code).is_at_most(100) }
      it { is_expected.to validate_uniqueness_of(:code) }
      it { is_expected.to validate_presence_of(:code) }
    end

    describe '#uri' do
      it { is_expected.to validate_length_of(:uri).is_at_most(250) }
      it { is_expected.to validate_presence_of(:uri) }
    end
  end
end
