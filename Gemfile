# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.3'

# Base courier handler.
gem 'courier_handlers',
    git: 'https://gitlab.com/courier_bot/courier_handlers/base_handler.git'

# Base courier handler for emails.
gem 'courier_handlers-email_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/email_handler.git'

# Courier handler for the SendGrid third-party service.
gem 'courier_handlers-sendgrid_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/sendgrid_handler.git'

# Base courier handler for text messages.
gem 'courier_handlers-text_message_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/text_message_handler.git'

# Courier handler for the Twilio third-party service.
gem 'courier_handlers-twilio_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/twilio_handler.git'

# Allows HTTP requests.
gem 'faraday'

# Postgresql database to store all records.
gem 'pg'

# The project's server.
gem 'puma'

# The project's framework.
gem 'rails', '~> 5.2'

# Allows the project to run background jobs.
gem 'resque'

group :development do
  # Allows the server to automatically stay updated to changes in the code.
  gem 'listen'
end

group :test do
  # Builds fake models based on templates.
  gem 'factory_bot_rails'

  # Generates fake values.
  gem 'faker'

  # Mocks a Redis database. This prevents the tests from requiring a running Redis daemon.
  gem 'mock_redis'

  # Allows for mutation testing. This gem is handled by bundler because it needs to know about
  # `rspec` and all other gem versions.
  gem 'mutant-rspec', require: false

  # Allows for general testing. All tests are in the `spec/` folder.
  gem 'rspec-rails'

  # Adds many Rails-related spec matchers.
  gem 'shoulda'

  # Allows for code coverage during testing. This is included in `spec/rails_helper.rb`.
  gem 'simplecov', require: false

  # Prevents all HTTP requests from leaving the tests.
  gem 'webmock'
end
