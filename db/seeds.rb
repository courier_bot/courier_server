# frozen_string_literal: true

client = Client.create!(code: 'client_1', uri: 'http://example.com/courier_server/contact_info')

order_created_message_kind = MessageKind.create!(code: 'order_created', client: client)
order_shipped_message_kind = MessageKind.create!(code: 'order_shipped', client: client)

email_delivery_kind = DeliveryKind.create!(code: 'sendgrid_email',
                                           contact_kind: 'email',
                                           handler_type: 'CourierHandlers::SendgridHandler')

text_message_delivery_kind = DeliveryKind.create!(code: 'twilio_text_message',
                                                  contact_kind: 'text_message',
                                                  handler_type: 'CourierHandlers::TwilioHandler')
