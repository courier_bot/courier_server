# frozen_string_literal: true

class CreateDeliveryKinds < ActiveRecord::Migration[5.2]
  def change
    rename_table :handler_kinds, :delivery_kinds

    change_table :delivery_kinds, bulk: true do |t|
      t.string :kind, limit: 100, null: false, index: true
      t.string :handler_type, limit: 100, null: false
    end

    rename_column :deliveries, :handler_kind_id, :delivery_kind_id
  end
end
