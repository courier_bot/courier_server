# frozen_string_literal: true

class AddStatusToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :status, :string, limit: 100, null: false, default: 'delivered'
    add_index :messages, :status
  end
end
