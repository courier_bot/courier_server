# frozen_string_literal: true

class AddUriToMessageKinds < ActiveRecord::Migration[5.2]
  def change
    add_column :message_kinds, :uri, :string, limit: 250, null: false, default: ''
  end
end
