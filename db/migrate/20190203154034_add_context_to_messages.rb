# frozen_string_literal: true

class AddContextToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :context, :string, limit: 250, null: true
  end
end
