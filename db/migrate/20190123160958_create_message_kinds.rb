# frozen_string_literal: true

class CreateMessageKinds < ActiveRecord::Migration[5.2]
  def change
    create_table :message_kinds do |t|
      t.string :code, limit: 100, null: false
      t.timestamps
    end

    add_index :message_kinds, :code, unique: true
    add_reference :messages, :message_kind, foreign_key: true, index: true, null: false, default: 0
  end
end
