class AddDeletedToMessageKinds < ActiveRecord::Migration[5.2]
  def change
    add_column :message_kinds, :deleted, :bool, default: false
  end
end
