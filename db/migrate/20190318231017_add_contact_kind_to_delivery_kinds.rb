# frozen_string_literal: true

class AddContactKindToDeliveryKinds < ActiveRecord::Migration[5.2]
  def change
    add_column :delivery_kinds, :contact_kind, :string, limit: 100, null: false, default: ''
  end
end
