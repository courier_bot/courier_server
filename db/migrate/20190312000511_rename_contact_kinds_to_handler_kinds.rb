# frozen_string_literal: true

class RenameContactKindsToHandlerKinds < ActiveRecord::Migration[5.2]
  def change
    rename_table :contact_kinds, :handler_kinds
    rename_column :deliveries, :contact_kind_id, :handler_kind_id
  end
end
