# frozen_string_literal: true

class RenameServiceTypeToHandlerType < ActiveRecord::Migration[5.2]
  def change
    rename_column :contact_kinds, :service_type, :handler_type
  end
end
