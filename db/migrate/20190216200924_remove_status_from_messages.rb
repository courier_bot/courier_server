# frozen_string_literal: true

class RemoveStatusFromMessages < ActiveRecord::Migration[5.2]
  def change
    remove_column :messages, :status, :string, limit: 100, null: false, default: 'delivered'
  end
end
