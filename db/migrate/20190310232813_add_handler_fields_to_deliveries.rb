# frozen_string_literal: true

class AddHandlerFieldsToDeliveries < ActiveRecord::Migration[5.2]
  def up
    change_table :deliveries, bulk: true do |t|
      t.remove :contact_value
      t.string :handler_fields, null: false, default: '{}'
    end
  end

  def down
    change_table :deliveries, bulk: true do |t|
      t.remove :handler_fields
      t.string :contact_value, null: false, default: ''
    end
  end
end
