# frozen_string_literal: true

class RemoveKindFromDeliveryKinds < ActiveRecord::Migration[5.2]
  def change
    remove_column :delivery_kinds, :kind, :string, limit: 100
  end
end
