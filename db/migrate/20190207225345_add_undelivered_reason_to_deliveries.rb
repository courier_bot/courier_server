# frozen_string_literal: true

class AddUndeliveredReasonToDeliveries < ActiveRecord::Migration[5.2]
  def change
    add_column :deliveries, :undelivered_reason, :string, limit: 4096, null: true
  end
end
