# frozen_string_literal: true

class CreateDeliveryFields < ActiveRecord::Migration[5.2]
  def change
    create_table :delivery_fields do |t|
      t.references :delivery
      t.string :name, limit: 100, null: false
      t.string :value, limit: 1024, index: true
      t.timestamps
    end

    remove_column :deliveries, :handler_fields, :string, null: false, default: '{}'
  end
end
