# frozen_string_literal: true

class RemoveHandlerType < ActiveRecord::Migration[5.2]
  def change
    remove_column :contact_kinds, :handler_type, :string, limit: 250, null: false, default: ''
  end
end
