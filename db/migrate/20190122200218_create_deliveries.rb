# frozen_string_literal: true

class CreateDeliveries < ActiveRecord::Migration[5.2]
  def change
    create_table :deliveries do |t|
      t.string :contact_kind, null: false
      t.string :contact_value, null: false
      t.references :message, foreign_key: true, index: true, null: false
      t.timestamps
      t.datetime :delivered_at, null: true
    end

    remove_column :messages, :sent_at, type: :datetime
  end
end
