# frozen_string_literal: true

class AddExternalIdToDeliveries < ActiveRecord::Migration[5.2]
  def change
    add_column :deliveries, :external_id, :string, limit: 250, null: true
  end
end
