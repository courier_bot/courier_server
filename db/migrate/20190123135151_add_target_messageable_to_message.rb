# frozen_string_literal: true

class AddTargetMessageableToMessage < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :target_messageable, :string, limit: 250, null: false, default: 'none'
    add_index :messages, :target_messageable
  end
end
