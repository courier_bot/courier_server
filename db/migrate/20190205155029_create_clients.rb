# frozen_string_literal: true

class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :code, limit: 100, null: false
      t.string :uri, limit: 250, null: false
      t.timestamps
    end

    remove_column :message_kinds, :uri, :string, limit: 250, null: false, default: ''
    add_reference :message_kinds, :client
  end
end
