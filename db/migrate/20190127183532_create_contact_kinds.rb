# frozen_string_literal: true

class CreateContactKinds < ActiveRecord::Migration[5.2]
  def change
    create_table :contact_kinds do |t|
      t.string :code, limit: 100, null: false
      t.string :service_type, limit: 250, null: false
      t.timestamps
    end

    add_index :contact_kinds,
              :code,
              unique: true

    remove_column :deliveries,
                  :contact_kind,
                  :string

    add_reference :deliveries,
                  :contact_kind,
                  foreign_key: true,
                  index: true,
                  null: false,
                  default: 0
  end
end
