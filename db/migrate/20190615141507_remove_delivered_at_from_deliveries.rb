class RemoveDeliveredAtFromDeliveries < ActiveRecord::Migration[5.2]
  def change
    remove_column :deliveries, :delivered_at, :datetime, null: true
  end
end
