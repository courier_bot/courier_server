# frozen_string_literal: true

class AddStatusToDeliveries < ActiveRecord::Migration[5.2]
  def change
    add_column :deliveries, :status, :string, limit: 100, null: false, default: 'delivered'
    add_index :deliveries, :status
  end
end
