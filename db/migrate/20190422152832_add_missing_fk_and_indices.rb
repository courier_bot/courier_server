class AddMissingFkAndIndices < ActiveRecord::Migration[5.2]
  def change
    add_index :clients, :code
    add_index :deliveries, :external_id
    add_index :delivery_kinds, :contact_kind
    add_index :messages, :context

    add_foreign_key :message_kinds, :clients
    add_foreign_key :delivery_fields, :deliveries
  end
end
