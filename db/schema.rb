# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_30_001621) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.string "uri", limit: 250, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_clients_on_code"
  end

  create_table "deliveries", force: :cascade do |t|
    t.bigint "message_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "delivery_kind_id", default: 0, null: false
    t.string "status", limit: 100, default: "delivered", null: false
    t.string "undelivered_reason", limit: 4096
    t.string "external_id", limit: 250
    t.index ["delivery_kind_id"], name: "index_deliveries_on_delivery_kind_id"
    t.index ["external_id"], name: "index_deliveries_on_external_id"
    t.index ["message_id"], name: "index_deliveries_on_message_id"
    t.index ["status"], name: "index_deliveries_on_status"
  end

  create_table "delivery_fields", force: :cascade do |t|
    t.bigint "delivery_id"
    t.string "name", limit: 100, null: false
    t.string "value", limit: 1024
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["delivery_id"], name: "index_delivery_fields_on_delivery_id"
    t.index ["value"], name: "index_delivery_fields_on_value"
  end

  create_table "delivery_kinds", force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "handler_type", limit: 100, null: false
    t.string "contact_kind", limit: 100, default: "", null: false
    t.index ["code"], name: "index_delivery_kinds_on_code", unique: true
    t.index ["contact_kind"], name: "index_delivery_kinds_on_contact_kind"
  end

  create_table "message_kinds", force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "client_id"
    t.boolean "deleted", default: false
    t.index ["client_id"], name: "index_message_kinds_on_client_id"
    t.index ["code"], name: "index_message_kinds_on_code", unique: true
  end

  create_table "messages", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "target_messageable", limit: 250, default: "none", null: false
    t.bigint "message_kind_id", default: 0, null: false
    t.string "context", limit: 250
    t.string "status", limit: 100, default: "delivered", null: false
    t.index ["context"], name: "index_messages_on_context"
    t.index ["message_kind_id"], name: "index_messages_on_message_kind_id"
    t.index ["status"], name: "index_messages_on_status"
    t.index ["target_messageable"], name: "index_messages_on_target_messageable"
  end

  add_foreign_key "deliveries", "delivery_kinds"
  add_foreign_key "deliveries", "messages"
  add_foreign_key "delivery_fields", "deliveries"
  add_foreign_key "message_kinds", "clients"
  add_foreign_key "messages", "message_kinds"
end
