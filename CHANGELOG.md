# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Route to retrieve a message by its id.
- Routes related to message kinds.
- Added the `deleted` attribute to `MessageKind`.
- Route to retrieve all messages.
- Routes related to clients.

### Changed
- Temporarily simplified the error handling.

### Removed
- `Delivery`'s `delivered_at` attribute.
- Removed `MessageService` and `MessageKindService`.

## [0.3](https://gitlab.com/courier_bot/courier_server/tags/v0.3) - 2019-06-13

### Added
- Kubernetes deployment using Helm.

### Changed
- Moved all database, credentials, and defaults configurations to environment variables.

### Removed
- Removed immediate deliveries (priority queues will later be used).
- Removed delivery field normalization from the server (only in the handlers now).

## [0.2](https://gitlab.com/courier_bot/courier_server/tags/v0.2) - 2019-05-09

### Added
- Allow immediate deliveries (no background queues).
- Add new status `unconfirmed` with its `undelivered_reason` attribute to deliveries.
- Add `external_id` attribute to deliveries.
- Implement third-party service callbacks.
- The message is now queued back for delivery when a callback response is negative.
- `DeliveryKind`'s `contact_kind` attribute.
- Handlers are in charge of normalizing fields, through the `normalize_fields` method.
- New `DeliveryResponseService` and `CallbackRequestService` classes.
- Add `Message` status `queued`.

### Changed
- Update the server to use the new courier handlers.
- Use the new handler registry to find handlers.
- Response for POST /messages request is now more complete.
- Callback requests are now processed in an async queue.
- `Delivery` model's `contact_value` is now a `Hash` object under `handler_fields`.
- `ContactInfoResponse`'s handler arguments `args` are now called `fields`.
- `Delivery` model's `contact_kind` is now named `handler_kind`.
- `handler_fields` are now served by the `DeliveryFields` database table.
- `HandlerKind` model renamed to `DeliveryKind`, with more attributes.
- Rename `ContactInfoService` to more appropriately-named `ClientService`.

### Removed
- Removed `DeliverKind`'s `kind` attribute.

## [0.1](https://gitlab.com/courier_bot/courier_server/tags/v0.1) - 2019-02-06

### Added
- All basic models - `Message`, `MessageKind`, `Delivery`, `ContactKind`, and `Client`.
- Messages controller to receive POST requests for new messages to be sent.
- Requests to clients for more information about a specific message that is about to be sent.
- Background job to send a message.
- Background job to periodically look for new messages to send.
- Courier services for email ([SendGrid](https://sendgrid.com/)) and text messages ([Twilio](https://www.twilio.com/)) included by default.
