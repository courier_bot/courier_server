# frozen_string_literal: true

desc 'Periodically finds new pending messages to deliver'
task periodic_message_deliveries: [:environment] do
  loop do
    PeriodicMessageDeliveriesJob.perform_now
    sleep Integer(ENV['PERIODIC_MESSAGE_DELIVERIES_SLEEP'] || '5', 10)
  end
end
