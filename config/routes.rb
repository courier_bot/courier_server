# frozen_string_literal: true

Rails.application.routes.draw do
  resources :clients, only: %i[index create show update destroy]
  resources :messages, only: %i[index create show]
  resources :message_kinds, only: %i[index create show update destroy]

  post '/callbacks/:contact_kind', to: 'callbacks#create'
end
