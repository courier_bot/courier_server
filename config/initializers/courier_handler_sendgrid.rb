# frozen_string_literal: true

CourierHandlers::SendgridHandler.configure do |config|
  config.api_key = ENV['SENDGRID_API_KEY']
  config.default_from = ENV['SENDGRID_DEFAULT_FROM_EMAIL_ADDRESS']
end
