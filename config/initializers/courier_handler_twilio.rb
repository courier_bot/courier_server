# frozen_string_literal: true

CourierHandlers::TwilioHandler.configure do |config|
  config.account_sid = ENV['TWILIO_ACCOUNT_SID']
  config.auth_token = ENV['TWILIO_AUTH_TOKEN']
  config.default_from = ENV['TWILIO_DEFAULT_FROM_PHONE_NUMBER']
end
