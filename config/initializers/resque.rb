# frozen_string_literal: true

# This file is also used as a config file when starting `resque-web`:
#
#     $ resque-web config/initializers/resque.rb
#
require 'resque'

redis_host = ENV['REDIS_HOST'] || 'redis'
redis_port = ENV['REDIS_PORT'] || '6379'
redis = "#{redis_host}:#{redis_port}"

if defined? Rails
  redis = MockRedis.new if Rails.env.test?
  log_path = Rails.root.join('log', "resque_#{Rails.env}.log")
else
  log_path = File.join(File.dirname(__FILE__), 'resque.log')
end

Resque.redis = redis
Resque.logger = Logger.new(log_path, level: Logger::INFO)
